export const dropdowns = {
    thesis: {
        id: "thesis",
        title: "Teza",
        color: "#007693",
    },
    resolutions: {
        id: "resolutions",
        title: "Rozstrzygnięcia",
        color: "#50EAAD",
    },
    // legal_basis: {
    //     id: "legal_basis",
    //     title: "Podstawa prawna",
    //     color: "#3e9b27",
    // },
    allegations: {
        id: "allegations",
        title: "Roszczenia / zarzuty",
        color: "#9A8BEE",
    },
    facts: {
        id: "facts",
        title: "Fakty",
        color: "#FF8080",
    },
    legal_argumentations: {
        id: "legal_argumentations",
        title: "Argumentacja",
        color: "#0086FB",
    },
    cited_regulations: {
        id: "cited_regulations",
        title: "Przytoczone przepisy",
        color: "#b400b3",
    },
    cited_cases: {
        id: "cited_cases",
        title: "Przytoczone sprawy",
        color: "#beb116",
    },
};
