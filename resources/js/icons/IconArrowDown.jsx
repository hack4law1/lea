import React from 'react';
import PropTypes from "prop-types";

export const IconArrowDown = ({color, directionUp}) => {
    const className = "icon " + (directionUp ? "rotate180" : "");
    return (
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={className}>
            <g id="chevron-down-solid 3">
                <path id="Vector" d="M10.9407 4.94065C11.5267 4.35471 12.4782 4.35471 13.0642 4.94065L22.0642 13.9406C22.6501 14.5266 22.6501 15.4781 22.0642 16.0641C21.4782 16.65 20.5267 16.65 19.9407 16.0641L12.0001 8.12346L4.05947 16.0594C3.47354 16.6453 2.52197 16.6453 1.93604 16.0594C1.3501 15.4735 1.3501 14.5219 1.93604 13.936L10.936 4.93596L10.9407 4.94065Z" fill={color}/>
            </g>
        </svg>
    );
};

IconArrowDown.propTypes = {
    color: PropTypes.string,
    directionUp: PropTypes.bool
}

export default IconArrowDown;
