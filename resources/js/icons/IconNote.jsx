import React from 'react';

const IconNote = () => {
    return (
        <svg width="21" height="24" viewBox="0 0 21 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3 1.5C1.34531 1.5 0 2.84531 0 4.5V19.5C0 21.1547 1.34531 22.5 3 22.5H13.5V17.25C13.5 16.0078 14.5078 15 15.75 15H21V4.5C21 2.84531 19.6547 1.5 18 1.5H3ZM21 16.5H18.8766H15.75C15.3375 16.5 15 16.8375 15 17.25V20.3766V22.5L16.5 21L19.5 18L21 16.5Z" fill="#808080"/>
        </svg>
    );
};

export default IconNote;
