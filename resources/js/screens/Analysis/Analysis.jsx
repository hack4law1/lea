import React, {useState} from 'react';
import Header from "../../components/common/Header";
import Container from "../../components/common/Container";
import Content from "../../components/common/Content";
import "./Analysis.scss";
import AnalysisJudgementPreview from "../../components/analysis/AnalysisJudgementPreview/AnalysisJudgementPreview";
import IconDocument from "@/icons/IconDocument";
import Tabs from "@/components/analysis/Tabs/Tabs";
import AnalysisJudgementLine from "@/components/analysis/AnalysisJudgementLine/AnalysisJudgementLine";

const Analysis = () => {
    const title = "Sprawa jakaś tam o coś tam";
    const [selectedTab, setSelectedTab] = useState(1);

    return (
        <Container>
            <Header/>
            <Content>
                <div className={"analysis"}>
                    <div className={"analysis__top"}>
                        <Tabs onTabClick={(newTab) => setSelectedTab(newTab)}
                              selected={selectedTab}
                        />
                        <div className={"details"}>
                            <span>{title}</span>
                            <IconDocument/>
                        </div>
                    </div>
                    <div className={"analysis__tabs-content"}>
                        {selectedTab === 1 ? <AnalysisJudgementPreview /> : <AnalysisJudgementLine />}
                    </div>
                </div>
            </Content>
        </Container>
    );
};

export default Analysis;
