import React from 'react';
import {
    createBrowserRouter,
} from "react-router-dom";

import Dashboard from "../screens/Dashboard/Dashboard";
import Search from "../screens/Search/Search";
import Analysis from "../screens/Analysis/Analysis";
import Login from "../screens/Login";
import Register from "../screens/Register";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Dashboard />,
    },
    {
        path: "/search",
        element: <Search />,
    },
    {
        path: "/analysis",
        element: <Analysis />,
    },
    {
        path: "/login",
        element: <Login />,
    },
    {
        path: "/register",
        element: <Register />,
    },
]);

export default router;
