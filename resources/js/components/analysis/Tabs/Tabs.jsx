import React from 'react';
import PropTypes from 'prop-types';
import "./Tabs.scss";

const Tabs = props => {
    return (
        <div className={"tabs"}>
            <div className={`tabs__tab ${props.selected === 1 ? "tabs__tab--active" : ""}`}
                 onClick={() => props.onTabClick(1)}>Podgląd orzeczenia
            </div>
            <div className={"tabs__spacer"}/>
            <div className={`tabs__tab ${props.selected === 2 ? "tabs__tab--active" : ""}`}
                 onClick={() => props.onTabClick(2)}>Linia orzeczenia
            </div>
        </div>
    );
};

Tabs.propTypes = {
    selected: PropTypes.number,
    onTabClick: PropTypes.func
};

export default Tabs;
