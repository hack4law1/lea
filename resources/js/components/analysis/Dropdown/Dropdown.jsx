import React, {useState} from 'react';
import PropTypes from 'prop-types';
import IconArrowDown from "../../../icons/IconArrowDown";
import "./Dropdown.scss";

const Dropdown = props => {
    return (
        <div className={`dropdown`}>
            <div className={"dropdown__header"} onClick={() => props.onClick()}>
                <span style={{'color': props.color}}>{props.title}</span>
                <IconArrowDown color={props.color} directionUp={!props.isOpen}/>
            </div>
            <div className={`dropdown__content ${props.isOpen ? "dropdown__content--open" : ""}`}>
                {props.isOpen && props.content}
            </div>
        </div>
    );
};

Dropdown.propTypes = {
    title: PropTypes.string,
    color: PropTypes.string,
    content: PropTypes.node,
    onContentHover: PropTypes.func,
    isOpen: PropTypes.bool,
    onClick: PropTypes.func
};

export default Dropdown;
