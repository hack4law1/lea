import React from 'react';
import "./AnalysisJudgementLine.scss";
import {Link} from "react-router-dom";

const AnalysisJudgementLine = props => {

    return (
        <div className={"judgement-line"}>
            <div className={"judgement-line__header"}>
                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.
            </div>
            <div className={"judgement-line__content"}>
                <div className={"line"}>
                    <div className={"line__title"}>Pierwsza linia orzecznicza</div>
                    <div className={"line__items"}>
                        <div className={"line__item item"}>
                            <div className={"item__title"}>LOREM IPSUM POTĘŻNE</div>
                            <div className={"item__text"}>
                                Litwo ojczyzno moja ty jesteś jak zdrowie ile czegos tam masz każdy się dowie co cię stracił.
                            </div>
                            <div className={"item__link"}>
                                <Link to={""}>Zobacz orzeczenie</Link>
                            </div>
                        </div>
                        <div className={"line__item item"}>
                            <div className={"item__title"}>LOREM IPSUM POTĘŻNE</div>
                            <div className={"item__text"}>
                                Litwo ojczyzno moja ty jesteś jak zdrowie ile czegos tam masz każdy się dowie co cię stracił.
                            </div>
                            <div className={"item__link"}>
                                <Link to={""}>Zobacz orzeczenie</Link>
                            </div>
                        </div>
                        <div className={"line__item item"}>
                            <div className={"item__title"}>LOREM IPSUM POTĘŻNE</div>
                            <div className={"item__text"}>
                                Litwo ojczyzno moja ty jesteś jak zdrowie ile czegos tam masz każdy się dowie co cię stracił.
                            </div>
                            <div className={"item__link"}>
                                <Link to={""}>Zobacz orzeczenie</Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className={"line"}>
                    <div className={"line__title"}>Druga linia orzecznicza</div>
                    <div className={"line__items"}>
                        <div className={"line__item item"}>
                            <div className={"item__title"}>LOREM IPSUM POTĘŻNE</div>
                            <div className={"item__text"}>
                                Litwo ojczyzno moja ty jesteś jak zdrowie ile czegos tam masz każdy się dowie co cię stracił.
                            </div>
                            <div className={"item__link"}>
                                <Link to={""}>Zobacz orzeczenie</Link>
                            </div>
                        </div>
                        <div className={"line__item item"}>
                            <div className={"item__title"}>LOREM IPSUM POTĘŻNE</div>
                            <div className={"item__text"}>
                                Litwo ojczyzno moja ty jesteś jak zdrowie ile czegos tam masz każdy się dowie co cię stracił.
                            </div>
                            <div className={"item__link"}>
                                <Link to={""}>Zobacz orzeczenie</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

AnalysisJudgementLine.propTypes = {};

export default AnalysisJudgementLine;
