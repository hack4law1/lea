import React from 'react';
import PropTypes from "prop-types";

const ButtonWithIcon = (props) => {
    return (
        <div className={"button button--with-icon"}>
            <span>{props.text}</span>
            <div className={"icon"}>{props.icon}</div>
        </div>
    );
};

ButtonWithIcon.propTypes = {
    text: PropTypes.string,
    icon: PropTypes.node
}

export default ButtonWithIcon;
