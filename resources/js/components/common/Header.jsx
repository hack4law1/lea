import React from 'react';
import {Link, useLocation} from "react-router-dom";

import Logo from "../../../assets/logo.svg";
import IconDashboard from "../../icons/IconDashboard";
import IconDocument from "../../icons/IconDocument";
import IconNotes from "../../icons/IconNote";
import IconPrint from "../../icons/IconPrint";
import IconQuote from "../../icons/IconQuote";
import ButtonWithIcon from "@/components/common/ButtonWithIcon";
import IconUser from "@/icons/IconUser";

const Header = (props) => {
        const location = useLocation();

        return (
            <header className={"header"}>
                <div className={"header__logo"}>
                    <img src={Logo} alt={"Logo"}/>
                </div>
                {location.pathname === "/" || location.pathname === "/search" ?
                    <div className={"header__search"}>
                        <input type={"search"} placeholder={"Szukaj..."} />
                    </div>
                    :
                    <nav className={"header__menu"}>
                        <ul>
                            <li>
                                <Link to="/">
                                    <IconDashboard/>
                                </Link>
                            </li>
                            <li>
                                <Link to="/search">
                                    <IconDocument/>
                                </Link>
                            </li>
                            <li>
                                <IconNotes/>
                            </li>
                            <li>
                                <IconPrint/>
                            </li>
                            <li>
                                <IconQuote/>
                            </li>
                        </ul>
                    </nav>
                }
                <div className={"header__actions"}>
                    <ButtonWithIcon text={"Login"} icon={<IconUser />} />
                </div>
            </header>
        )
            ;
    }
;

export default Header;
