import dashboard from './assets/house-solid.svg';
import search from './assets/search-solid.svg';
import document from './assets/file-lines-regular.svg';
import stat1 from './assets/frame38.png';
import stat2 from './assets/frame39.png';
import stat3 from './assets/frame40.png';
import analise from './assets/go-to-analysis.svg';

export default {
    dashboard: dashboard,
    search: search,
    document: document, 
    stat1: stat1,
    stat2: stat2,
    stat3: stat3,
    analise: analise,
}