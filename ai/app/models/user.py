from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql import func

from .base import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    email = Column(String)
    email_verified_at = Column(DateTime)  # Timestamp
    password = Column(String)
    remember_token = Column(String(length=100))
    created_at = Column(DateTime, server_default=func.utcnow())  # Timestamp
    updated_at = Column(DateTime, onupdate=func.utcnow())  # Timestamp

    def __repr__(self):
        return f"[{self.id}] {self.name}"
