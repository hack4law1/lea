from config.database import SessionLocal
from app.models.user import User
from app.services.test_service import TestService

session = SessionLocal()
test_service = TestService()


class TestController:
    def test_action(self):
        return [session.query(User).get(1)]

    def use_service(self):
        return test_service.do_a_backflip()


test_controller = TestController()
