from langchain.chat_models import ChatOpenAI
from langchain.llms import OpenAI
from langchain.chains import create_tagging_chain, create_extraction_chain
from langchain.prompts import ChatPromptTemplate

# a = OpenAI(temperature=0, model="gpt-3.5-turbo-0613")
# llm = ChatOpenAI(temperature=0, model="gpt-3.5-turbo-0613")
llm = ChatOpenAI(temperature=0, model="gpt-4")
schema = {
    "properties": {
        "type": {
            "type": "string",
            "enum": ["fact", "claim", "allegation", "resolution", "legal_argumentation", "thesis"],
            "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. fact to stwierdzenie stanu rzeczy. claim to roszczenie. allegation to zarzut przez, które rozumie się zarówno odmowę spełnienia roszczenia jak i zarzut skierowany do osoby podejrzanej. resolution to rozstrzygnięcie, a więc decyzje sądu. legal_argumentation to argumentacja prawna, a więc argumentacja w oparciu o przepisy prawne, orzecznictwa, doktryny prawa oraz inne źródła prawa. thesis czyli teza to główna konkluzja lub stwierdzenie prawne zawarte w uzasadnieniu wyroku lub postanowienia sądowego."
        }
    }
}

chain = create_tagging_chain(schema, llm)
# print(chain.prompt.messages[0].prompt.template)

# schema = {
#     "properties": {
#         "fact": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. fact to stwierdzenie stanu rzeczy."
#         },
#         "claim": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. claim to roszczenie."
#         },
#         "allegation": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. allegation to zarzut przez, które rozumie się zarówno odmowę spełnienia roszczenia jak i zarzut skierowany do osoby podejrzanej."
#         },
#         "resolution": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. resolution to rozstrzygnięcie, a więc decyzje sądu."
#         },
#         "legal_argumentation": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. legal_argumentation to argumentacja prawna, a więc argumentacja w oparciu o przepisy prawne, orzecznictwa, doktryny prawa oraz inne źródła prawa."
#         },
#         "thesis": {
#             "type": "string",
#             "description": "Opis funkcji, którą pełni zdanie w orzecznictwie sądowym. thesis czyli teza to główna konkluzja lub stwierdzenie prawne zawarte w uzasadnieniu wyroku lub postanowienia sądowego."
#         },
#     }
# }
#
# chain = create_extraction_chain(schema, llm)

# prompt = """
# Poniżej znajduje się wycinek z orzecznictwa sądowego pochodzącego z polskiego sądu.
# Proszę Cię abyś wyszukał w danym tekście zdania, które pasują do jednej z kategorii: Zarzut, Rozstrzygnięcie, Fakt, Argumentacja Prawna, Teza.
# Wynik swojej operacji podaj jako obiekt JSON, w którym do konkretnych kategorii powinna być przypisana tablica z numerami zdań pasującymi do kategorii.
# Każde zdanie może pasować maksymalnie do jednej kategorii, z czego nie musi pasować do żadnej. Zawsze podawaj kategorię, której jesteś najbardziej pewien.
#
# Wycinek orzecznictwa:
# Sąd Apelacyjny zważył co następuje. Apelacja nie zasługuje na uwzględnienie. W pierwszym rzędzie należy podkreślić że wbrew zarzutowi apelacji Sąd Okręgowy wprost zaznaczył, że niewiarygodne są zeznania powódki złożone w niniejszej sprawie, konfrontowane z jej zeznaniami złożonymi w charakterze świadka w postępowaniu karnym i wprost przyjął, że miała ona świadomość jazdy samochodem z kierowcą, z którym wcześniej spożywała alkohol.
# A limine wadliwy jest zarzut apelata, że w tym kontekście Sąd I instancji naruszył przepis prawa procesowego wadliwie ustalając stopień (wysokość) przyczynienia się powódki do powstania szkody. Kwestia przyczynienia się poszkodowanego do powstania szkody jest zagadnieniem rozważanym na tle przepisu art. 362 k.c., zatem należy do kwestii prawa materialnego a nie procesowaego, aktualizuje się dopiero na etapie oceny, czy przy ustalonej krzywdzie poszkodowanego i kwocie zadośćuczynienia ją kompensującego, samoświadczenie należne uprawnionemu winno zostać zmiarkowane w trybie art. 362 k.p.c.
# """

# prompt = """
# Poniżej znajduje się wycinek z orzecznictwa sądowego pochodzącego z polskiego sądu.
# Proszę Cię abyś wyszukał w danym tekście zdania, które pasują do jednej z kategorii: Zarzut, Rozstrzygnięcie, Fakt, Argumentacja Prawna, Teza.
# Wynik swojej operacji podaj jako obiekt JSON, w którym do konkretnych kategorii powinna być przypisana tablica ze zdaniami pasującymi do kategorii.
# Każde zdanie może pasować maksymalnie do jednej kategorii, z czego nie musi pasować do żadnej. Kategorie mogą zawierać kilka zdań. Zawsze podawaj kategorię, której jesteś najbardziej pewien.
#
# Wycinek orzecznictwa:
# Sąd Apelacyjny zważył co następuje. Apelacja nie zasługuje na uwzględnienie. W pierwszym rzędzie należy podkreślić że wbrew zarzutowi apelacji Sąd Okręgowy wprost zaznaczył, że niewiarygodne są zeznania powódki złożone w niniejszej sprawie, konfrontowane z jej zeznaniami złożonymi w charakterze świadka w postępowaniu karnym i wprost przyjął, że miała ona świadomość jazdy samochodem z kierowcą, z którym wcześniej spożywała alkohol.
# A limine wadliwy jest zarzut apelata, że w tym kontekście Sąd I instancji naruszył przepis prawa procesowego wadliwie ustalając stopień (wysokość) przyczynienia się powódki do powstania szkody. Kwestia przyczynienia się poszkodowanego do powstania szkody jest zagadnieniem rozważanym na tle przepisu art. 362 k.c., zatem należy do kwestii prawa materialnego a nie procesowaego, aktualizuje się dopiero na etapie oceny, czy przy ustalonej krzywdzie poszkodowanego i kwocie zadośćuczynienia ją kompensującego, samoświadczenie należne uprawnionemu winno zostać zmiarkowane w trybie art. 362 k.p.c.
# """

# prompt = """
# Sąd Apelacyjny zważył co następuje. Apelacja nie zasługuje na uwzględnienie. W pierwszym rzędzie należy podkreślić że wbrew zarzutowi apelacji Sąd Okręgowy wprost zaznaczył, że niewiarygodne są zeznania powódki złożone w niniejszej sprawie, konfrontowane z jej zeznaniami złożonymi w charakterze świadka w postępowaniu karnym i wprost przyjął, że miała ona świadomość jazdy samochodem z kierowcą, z którym wcześniej spożywała alkohol.
# A limine wadliwy jest zarzut apelata, że w tym kontekście Sąd I instancji naruszył przepis prawa procesowego wadliwie ustalając stopień (wysokość) przyczynienia się powódki do powstania szkody. Kwestia przyczynienia się poszkodowanego do powstania szkody jest zagadnieniem rozważanym na tle przepisu art. 362 k.c., zatem należy do kwestii prawa materialnego a nie procesowaego, aktualizuje się dopiero na etapie oceny, czy przy ustalonej krzywdzie poszkodowanego i kwocie zadośćuczynienia ją kompensującego, samoświadczenie należne uprawnionemu winno zostać zmiarkowane w trybie art. 362 k.p.c.
# """

# prompt = """
# W pierwszym rzędzie należy podkreślić że wbrew zarzutowi apelacji Sąd Okręgowy wprost zaznaczył, że niewiarygodne są zeznania powódki złożone w niniejszej sprawie, konfrontowane z jej zeznaniami złożonymi w charakterze świadka w postępowaniu karnym i wprost przyjął, że miała ona świadomość jazdy samochodem z kierowcą, z którym wcześniej spożywała alkohol.
# """

# prompt = """
# A limine wadliwy jest zarzut apelata, że w tym kontekście Sąd I instancji naruszył przepis prawa procesowego wadliwie ustalając stopień (wysokość) przyczynienia się powódki do powstania szkody.
# """

# prompt = """
# Kwestia przyczynienia się poszkodowanego do powstania szkody jest zagadnieniem rozważanym na tle przepisu art. 362 k.c., zatem należy do kwestii prawa materialnego a nie procesowaego, aktualizuje się dopiero na etapie oceny, czy przy ustalonej krzywdzie poszkodowanego i kwocie zadośćuczynienia ją kompensującego, samoświadczenie należne uprawnionemu winno zostać zmiarkowane w trybie art. 362 k.p.c.
# """

prompt = """
Ocena ta odpowiada warunkom określonym przez prawo procesowe - Sąd pierwszej instancji oparł swoje przekonanie na dowodach prawidłowo przeprowadzonych, z zachowaniem zasady bezpośredności oraz dokonał ustaleń faktycznych na podstawie wszechstronnego i bardzo wnikliwego rozważenia zebraego w sprawie materiału.
"""

class TestService:
    def do_a_backflip(self):
        print(prompt)
        res = chain.run(prompt)
        return res
        # test = OpenAI(temperature=0.1)
        # answer = test(prompt)
        # print(answer)
        # return answer
        # return prompt
        # return chain.prompt.messages[0].prompt.template

# class TestService:
#     llm = ChatOpenAI(temperature=0, model="gpt-3.5-turbo-0613")
#
#     def __int__(self, schema):
#         self.schema = {
#             "properties": {
#                 "type": {
#                     "type": "enum"
#                 }
#             }
#         }
#
#         chain =
#
#     def do_a_backflip(self):
#         return 'no'
