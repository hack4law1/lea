import os
from dotenv import load_dotenv

load_dotenv()


class AppSettings:
    PROJECT_NAME: str = os.getenv("PROJECT_NAME", "LEA Microservice")
    PROJECT_VERSION: str = os.getenv("PROJECT_VERSION", "1.0.0")


app_settings = AppSettings()
