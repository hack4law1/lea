from fastapi import APIRouter
from app.http.controllers.test import test_controller

router = APIRouter()


@router.get("/test")
def test():
    return test_controller.test_action()


@router.get("/service")
def service():
    return test_controller.use_service()
