<?php

namespace Database\Seeders;

use App\Enums\CourtTypeEnum;
use App\Models\CourtType;
use Illuminate\Database\Seeder;

class CourtTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        CourtType::insert([
            [
                'id' => 1,
                'name' => CourtTypeEnum::REGIONAL->name,
            ],
            [
                'id' => 2,
                'name' => CourtTypeEnum::APPELLATE->name,
            ],
            [
                'id' => 3,
                'name' => CourtTypeEnum::DISTRICT->name,
            ],
            [
                'id' => 4,
                'name' => CourtTypeEnum::SUPREME_COURT->name,
            ],
            [
                'id' => 5,
                'name' => CourtTypeEnum::ADMINISTRATIVE_COURT->name,
            ],
            [
                'id' => 6,
                'name' => CourtTypeEnum::CONSTITUTIONAL_COURT->name,
            ],
            [
                'id' => 7,
                'name' => CourtTypeEnum::NATIONAL_CHAMBER_OF_APPEAL->name,
            ],
        ]);
    }
}
