<?php

namespace Database\Seeders;

use App\Enums\JudgmentTypeEnum;
use App\Models\JudgmentType;
use Illuminate\Database\Seeder;

class JudgmentTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        JudgmentType::insert([
            [
                'id' => 1,
                'name' => JudgmentTypeEnum::DECISION->name,
            ],
            [
                'id' => 2,
                'name' => JudgmentTypeEnum::RESOLUTION->name,
            ],
            [
                'id' => 3,
                'name' => JudgmentTypeEnum::SENTENCE->name,
            ],
            [
                'id' => 4,
                'name' => JudgmentTypeEnum::ENACTMENT->name,
            ],
            [
                'id' => 5,
                'name' => JudgmentTypeEnum::JUSTIFICATION->name,
            ],
        ]);
    }
}
