<?php

namespace Database\Seeders;

use App\Enums\JudgeRoleEnum;
use App\Models\JudgeRole;
use Illuminate\Database\Seeder;

class JudgeRoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        JudgeRole::insert([
            [
                'id' => 1,
                'name' => JudgeRoleEnum::LEADING_JUDGE->name,
            ],
            [
                'id' => 2,
                'name' => JudgeRoleEnum::REPORTING_JUDGE->name,
            ],
            [
                'id' => 3,
                'name' => JudgeRoleEnum::AUTHOR_OF_THE_JUSTIFICATION->name,
            ],
            [
                'id' => 4,
                'name' => JudgeRoleEnum::SQUAD_MEMBER->name,
            ],
        ]);
    }
}
