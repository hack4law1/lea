<?php

namespace Database\Seeders;

use App\Enums\DepartmentTypeEnum;
use App\Models\DepartmentType;
use Illuminate\Database\Seeder;

class DepartmentTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DepartmentType::insert([
            [
                'id' => 1,
                'name' => DepartmentTypeEnum::CIVIL->name,
            ],
            [
                'id' => 2,
                'name' => DepartmentTypeEnum::PUNITIVE->name,
            ],
        ]);
    }
}
