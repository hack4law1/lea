<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('judgment_keyword', function (Blueprint $table) {
            $table->id();
            $table->foreignId('judgment_id')->constrained();
            $table->foreignId('keyword_id')->constrained();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('judgment_keyword');
    }
};
