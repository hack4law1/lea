<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('judgments', function (Blueprint $table) {
            $table->unsignedInteger('view_counter')->after('court_case_id')->default(0);
        });
    }

    public function down()
    {
        Schema::table('judgments', function (Blueprint $table) {
            $table->dropColumn('view_counter');
        });
    }
};
