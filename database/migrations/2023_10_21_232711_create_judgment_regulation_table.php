<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('judgment_regulation', function (Blueprint $table) {
            $table->id();
            $table->foreignId('judgment_id')->constrained();
            $table->foreignId('regulation_id')->constrained();

            $table->unique(['judgment_id', 'regulation_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('judgment_regulation');
    }
};
