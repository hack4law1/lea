<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('analysis_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('judgment_id')->constrained();
            $table->json('facts')->nullable();
            $table->json('allegations')->nullable();
            $table->json('resolutions')->nullable();
            $table->json('legal_argumentation')->nullable();
            $table->json('thesis')->nullable();
            $table->text('sentence')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('analysis_results');
    }
};
