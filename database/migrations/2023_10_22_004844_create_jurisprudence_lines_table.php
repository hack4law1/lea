<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('jurisprudence_lines', function (Blueprint $table) {
            $table->id();
            $table->foreignId('root_judgment_id');
            $table->foreignId('judgment_id')->constrained();
            $table->unsignedTinyInteger('line');

            $table->foreign('root_judgment_id')->references('id')->on('judgments');
        });
    }

    public function down()
    {
        Schema::dropIfExists('jurisprudence_lines');
    }
};
