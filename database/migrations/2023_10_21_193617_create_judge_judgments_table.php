<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('judge_judgments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('judge_id')->constrained();
            $table->foreignId('judgment_id')->constrained();
            $table->foreignId('judge_role_id')->constrained();

            $table->unique(['judge_id', 'judgment_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('judge_judgments');
    }
};
