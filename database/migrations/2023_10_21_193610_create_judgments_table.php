<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('judgments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('court_id')->constrained();
            $table->foreignId('judgment_type_id')->constrained();
            $table->foreignId('department_type_id')->constrained();
            $table->string('court_case_id');
            $table->string('title');
            $table->text('content');
            $table->json('referenced_cases')->nullable();
            $table->timestamp('judgment_date');
        });
    }

    public function down()
    {
        Schema::dropIfExists('judgments');
    }
};
