<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('courts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('court_type_id')->constrained();
            $table->foreignId('city_id')->constrained();
        });
    }

    public function down()
    {
        Schema::dropIfExists('courts');
    }
};
