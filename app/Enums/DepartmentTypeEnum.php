<?php

namespace App\Enums;

enum DepartmentTypeEnum: string
{
    case CIVIL = 'Cywilny';
    case PUNITIVE = 'Karny';
}
