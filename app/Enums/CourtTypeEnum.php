<?php

namespace App\Enums;

enum CourtTypeEnum: string
{
    case REGIONAL = 'Okregowy';
    case APPELLATE = 'Apelacyjny';
    case DISTRICT = 'Rejonowy';
    case SUPREME_COURT = 'Sąd Najwyższy';
    case ADMINISTRATIVE_COURT = 'Sąd Administracyjny';
    case CONSTITUTIONAL_COURT = 'Trybunał Konstytucyjny';
    case NATIONAL_CHAMBER_OF_APPEAL = 'Krajowa Izba Odwoławcza';
}
