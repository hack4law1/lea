<?php

namespace App\Enums;

enum JudgeRoleEnum: string
{
    case LEADING_JUDGE = 'Przewodniczący składu';
    case REPORTING_JUDGE = 'Sędzia sprawozdawca';
    case AUTHOR_OF_THE_JUSTIFICATION = 'Autor uzasadnienia';
    case SQUAD_MEMBER = 'Członek składu';
}
