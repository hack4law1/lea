<?php

namespace App\Enums;

enum JudgmentTypeEnum: string
{
    case DECISION = 'Postanowienie';
    case RESOLUTION = 'Uchwała';
    case SENTENCE = 'Wyrok';
    case ENACTMENT = 'Zarządzenie';
    case JUSTIFICATION = 'Uzasadnienie';
}
