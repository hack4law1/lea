<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourtType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
