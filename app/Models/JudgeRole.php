<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JudgeRole extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
