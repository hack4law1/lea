<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Keyword extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function keywords(): BelongsToMany
    {
        return $this->belongsToMany(Judgment::class);
    }
}
