<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Regulation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function judgment(): BelongsToMany
    {
        return $this->belongsToMany(Judgment::class);
    }
}
