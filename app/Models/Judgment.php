<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Judgment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'court_case_id',
        'court_id',
        'judgment_type_id',
        'department_type_id',
        'court_case_id',
        'view_counter',
        'title',
        'content',
        'referenced_cases',
        'judgment_date',
    ];

    protected $casts = [
        'referenced_cases' => 'array',
    ];

    public function jurisprudenceLines(): HasMany
    {
        return $this->hasMany(JurisprudenceLine::class);
    }

    public function court(): BelongsTo
    {
        return $this->belongsTo(Court::class);
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(
            DepartmentType::class,
            'department_type_id',
            'id'
        );
    }

    public function keywords(): BelongsToMany
    {
        return $this->belongsToMany(Keyword::class);
    }

    public function regulations(): BelongsToMany
    {
        return $this->belongsToMany(Regulation::class);
    }

    public function analysis(): HasOne
    {
        return $this->hasOne(AnalysisResult::class);
    }
}
