<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class JurisprudenceLine extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'root_judgment_id',
        'judgment_id',
        'line',
    ];

    public function rootJudgment(): BelongsTo
    {
        return $this->belongsTo(
            Judgment::class,
            'root_judgment_id',
            'id'
        );
    }

    public function judgment(): BelongsTo
    {
        return $this->belongsTo(Judgment::class);
    }
}
