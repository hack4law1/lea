<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JudgeJudgment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'judges_id',
        'judgment_id',
        'judges_role_id',
    ];
}
