<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
