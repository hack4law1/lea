<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'department_type_id',
    ];

    public function court(): BelongsToMany
    {
        return $this->belongsToMany(Court::class);
    }

    public function departmentType(): BelongsTo
    {
        return $this->belongsTo(DepartmentType::class);
    }
}
