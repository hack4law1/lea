<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Region extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function region(): HasOne
    {
        return $this->hasOne(City::class);
    }
}
