<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AnalysisResult extends Model
{
    protected $fillable = [
        'judgment_id',
        'sentence',
        'facts',
        'allegations',
        'resolutions',
        'legal_argumentation',
        'thesis',
    ];

    protected $casts = [
        'facts' => 'array',
        'allegations' => 'array',
        'resolutions' => 'array',
        'legal_argumentation' => 'array',
        'thesis' => 'array',
    ];

    public function judgment(): BelongsTo
    {
        return $this->belongsTo(Judgment::class);
    }
}
