<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JudgmentType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
