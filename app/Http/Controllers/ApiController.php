<?php

namespace App\Http\Controllers;

use App\Enums\JudgmentTypeEnum;
use App\Http\Requests\GetJudgmentsRequest;
use App\Http\Requests\GetSingleJudgmentRequest;
use App\Models\AnalysisResult;
use App\Models\Judgment;
use App\Models\JurisprudenceLine;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function getJudgments(GetJudgmentsRequest $request)
    {
        $parameters = $request->validated();

        return Judgment::with(['keywords', 'department', 'court.courtType', 'court.city'])
            ->when(Arr::exists($parameters, 'date'), function ($query) use ($parameters) {
                $query->whereDate('judgment_date', $parameters['date']);
            })
            ->when(Arr::exists($parameters, 'region'), function ($query) use ($parameters) {
                $query->whereHas('court.city.region', function ($query) use ($parameters) {
                    $query->where('name', 'ILIKE', "%{$parameters['region']}%");
                });
            })
            ->when(Arr::exists($parameters, 'courtName'), function ($query) use ($parameters) {
                $query->whereHas('court', function ($query) use ($parameters) {
                    $query->where('name', 'ILIKE', "%{$parameters['courtName']}%");
                });
            })
            ->when(Arr::exists($parameters, 'departmentType'), function ($query) use ($parameters) {
                $query->whereHas('department', function ($query) use ($parameters) {
                    $query->where('name', 'ILIKE', "%{$parameters['departmentType']}%");
                });
            })
            ->when(Arr::exists($parameters, 'keywords'), function ($query) use ($parameters) {
                $keywordArray = explode(',', $parameters['keywords']);

                $query->whereHas('keywords', function ($query) use ($keywordArray) {
                    $query->whereIn('name', $keywordArray);
                });
            })
            ->get()
            ->map(function (Judgment $item) {
                return [
                    'id' => $item->id,
                    'judgmentDate' => $item->judgment_date,
                    'sentence' => $item->analysis?->sentence ?? '',
                    'departmentType' => $item->department->name,
                    'keywords' => $item->keywords->pluck('name')->toArray(),
                    'courtType' => $item->court->courtType->name,
                    'city' => $item->court->city->name,
                ];
            });
    }

    public function getSingleJudgment(GetSingleJudgmentRequest $request)
    {
        $parameters = $request->validated();

        return Judgment::with(['jurisprudenceLines', 'analysis', 'regulations'])
            ->whereId($parameters['judgmentId'])
            ->get()
            ->map(function (Judgment $item) {
                $analysis = [
                    'facts' => $item->analysis?->facts,
                    'allegations' => $item->analysis?->allegations,
                    'resolutions' => $item->analysis?->resolutions,
                    'legal_argumentation' => $item->analysis?->legal_argumentation,
                    'thesis' => $item->analysis?->thesis,
                ];

                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'content' => $item->content,
                    'sentence' => $item->analysis?->sentence ?? '',
                    'analysis' => $analysis,
                    'referencedCases' => $item->referenced_cases,
                    'referencedRegulations' => $item->regulations->pluck('name'),
                ];
            });
    }

    public function getDashboard()
    {
        // General stats
        $totalAmountOfJudgments = Judgment::count();
        $totalAmountOfJLines = JurisprudenceLine::select(['root_judgment_id', 'line'])
            ->groupBy('root_judgment_id', 'line')
            ->count();
        $totalAmountOfTheses = AnalysisResult::all()
            ->pluck('thesis')
            ->flatten(1)
            ->count();

        $generalStats = [
            'amountOfJudgments' => $totalAmountOfJudgments,
            'amountOfJLines' => $totalAmountOfJLines,
            'amountOfTheses' => $totalAmountOfTheses,
        ];

        // Stats per region
        $amountOfJudgmentsPerRegion = DB::select("
            SELECT r.name, COUNT(*)
            FROM judgments AS j
            JOIN courts co on j.court_id = co.id
            JOIN cities ct on co.city_id = ct.id
            JOIN regions r on ct.region_id = r.id
            GROUP BY r.id;
        ");

        // Stats per Judgment Type
        $amountOfJudgmentsPerJudgmentType = DB::select("
            SELECT jt.name, COUNT(*)
            FROM judgments AS j
            JOIN judgment_types jt on j.judgment_type_id = jt.id
            GROUP BY jt.name;
        ");

        // Blocks
        $newestJudgments = Judgment::latest('judgment_date')
            ->limit(5)
            ->get()
            ->map(function (Judgment $judgment) {
                return [
                    'id' => $judgment->id,
                    'title' => $judgment->title,
                    'sentence' => $judgment->analysis?->sentence,
                    'departmentType' => $judgment->department->name,
                    'judgmentDate' => $judgment->judgment_date,
                ];
            });
        $mostViewed = Judgment::orderBy('view_counter', 'desc')
            ->limit(5)
            ->get()
            ->map(function (Judgment $judgment) {
                return [
                    'id' => $judgment->id,
                    'title' => $judgment->title,
                    'sentence' => $judgment->analysis?->sentence,
                    'departmentType' => $judgment->department->name,
                    'judgmentDate' => $judgment->judgment_date,
                ];
            });

        $todaysSelection = [];
        $seed = (int) today()->format('Ymd');
        foreach (JudgmentTypeEnum::cases() as $idx => $case) {
            $todaysSelection[$case->name] = Judgment::where('judgment_type_id', $idx)
                ->inRandomOrder($seed)
                ->limit(1)
                ->get()
                ->map(function (Judgment $judgment) {
                    return [
                        'id' => $judgment->id,
                        'title' => $judgment->title,
                        'sentence' => $judgment->analysis?->sentence,
                        'departmentType' => $judgment->department->name,
                        'judgmentDate' => $judgment->judgment_date,
                    ];
                })
                ?->toArray();
        }

        $blocks = [
            'newest' => $newestJudgments,
            'popular' => $mostViewed,
            'selected' => $todaysSelection,
        ];

        return [
            'generalStats' => $generalStats,
            'judgmentsPerRegion' => $amountOfJudgmentsPerRegion,
            'judgmentsPerJudgmentType' => $amountOfJudgmentsPerJudgmentType,
            'blocks' => $blocks,
        ];
    }
}
