<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetSingleJudgmentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'judgmentId' => ['numeric', 'exists:judgments,id']
        ];
    }

    public function authorize()
    {
        return true;
    }
}
