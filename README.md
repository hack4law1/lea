# Lea

## Setup Laravel
```sh
# clone this repo
% git clone https://gitlab.com/hack4law1/lea.git

% composer install
% php artisan serve
```


## Setup React
npm required.
```sh
% npm install
% npm run dev
```
